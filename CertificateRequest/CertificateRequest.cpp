
// CertificateRequest.cpp : ���������� ��������� ������� ��� ����������.
//

#include "stdafx.h"
#include "CertificateRequest.h"
#include "CertificateRequestDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCertificateRequestApp

BEGIN_MESSAGE_MAP(CCertificateRequestApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// �������� CCertificateRequestApp

CCertificateRequestApp::CCertificateRequestApp()
{
	// TODO: �������� ��� ��������,
	// ��������� ���� ������ ��� ������������� � InitInstance
}


// ������������ ������ CCertificateRequestApp

CCertificateRequestApp theApp;


// ������������� CCertificateRequestApp

BOOL CCertificateRequestApp::InitInstance()
{
	CWinApp::InitInstance();


	// ������ �������� ���� ���������
	CCertificateRequestDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();

	return FALSE;
}

