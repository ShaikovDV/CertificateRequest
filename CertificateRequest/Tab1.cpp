// Tab1.cpp : implementation file
//

#include "stdafx.h"
#include "CertificateRequest.h"
#include "Tab1.h"
#include "CertificateRequestDlg.h"
#include "afxdialogex.h"
#include "OpenSSLManip.h"
#include "Work.h"
#include <fstream>


// CTab1 dialog

IMPLEMENT_DYNAMIC(CTab1, CDialogEx)

CTab1::CTab1(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTab1::IDD, pParent)
{
	
}

CTab1::~CTab1()
{
}

static UINT CreateCSRThread(LPVOID pParam);

void CTab1::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	//DDX_Control(pDX, IDC_EDIT2, CEdit_CN);
}


BEGIN_MESSAGE_MAP(CTab1, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CTab1::OnBnClickedButton1)
	ON_EN_SETFOCUS(IDC_CN, &CTab1::OnEnSetfocusEdit2)
	ON_EN_KILLFOCUS(IDC_CN, &CTab1::OnEnKillfocusEdit2)
	ON_WM_CTLCOLOR()
	ON_WM_SHOWWINDOW()
	ON_EN_CHANGE(IDC_PASSWORD, &CTab1::OnEnChangePassword)
END_MESSAGE_MAP()


// CTab1 message handlers

BOOL CTab1::OnInitDialog()
{
	//����������� �������� �� ���������.

	wchar_t currentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, currentDir);

	wstring wFolder = currentDir;
	string Folder(wFolder.begin(), wFolder.end());

	if(Folder[Folder.size()-1] != '\\')
	{
		Folder = Folder+"\\";
	}

	CEdit * pEdtCN = (CEdit *)GetDlgItem(IDC_CN);
	CEdit * pEdt_COMPANY = (CEdit *)GetDlgItem(IDC_COMPANY);
	CEdit * pEdt_OU = (CEdit *)GetDlgItem(IDC_OU);
	CEdit * pEdtCITY = (CEdit *)GetDlgItem(IDC_CITY);
	CEdit * pEdt_EMAIL = (CEdit *)GetDlgItem(IDC_EMAIL);

	// �������� ���������� ��������� �������� ��������, ������� ��������� ������ ���� ��������� � �����
	fstream FieldsFile;
	FieldsFile.open(Folder + "FieldsMapping", std::ios::in);
	
	// ���� ���� ����
	if(FieldsFile)
	{
		// ��������� ��������
		CString TextCN, TextC, TextOU, TextS, TextEmail;
		char* temp = new char[100];
		FieldsFile.getline(temp, 100);
		TextCN = temp;
		FieldsFile.getline(temp, 100);
		TextC = temp;
		FieldsFile.getline(temp, 100);
		TextOU = temp;
		FieldsFile.getline(temp, 100);
		TextS = temp;
		FieldsFile.getline(temp, 100);
		TextEmail = temp;
		
		delete[] temp;
		FieldsFile.close();

		// �������������� ��������� ���������� ����������
		pEdtCN->SetWindowTextW(TextCN);
		pEdt_COMPANY->SetWindowTextW(TextC);
		pEdt_OU->SetWindowTextW(TextOU);
		pEdtCITY->SetWindowTextW(TextS);
		pEdt_EMAIL->SetWindowTextW(TextEmail);
	}
	else
	{

		// ���� ����� �� ���������� ���, �� ��������� ������ ����������
		
		pEdtCN->SetWindowTextW(L"������ �.�.");

		
		pEdtCITY->SetWindowTextW(L"������");
	}
	CButton* pEdtR2 = (CButton *)GetDlgItem(IDC_RADIO2);
	pEdtR2->SetCheck(1);
	
	// ���������� �������� ���������� � true ��� ������ � ����������� ����
	CN_OK=OU_OK=C_OK=ST_OK=E_OK=PASS_OK=true;

	// ����������� ������ �� ���������

	m_normalcolor = RGB(255,255,255);
	m_redcolor = RGB(255,160,122);

	m_normalbrush.CreateSolidBrush(m_normalcolor);
	m_redbrush.CreateSolidBrush(m_redcolor);

	return true;
}

// ��������� ����� � �������
void CTab1::OnBnClickedButton1()
{
	// �������� ������ �� �������� ���������� � �������� ��������
	CString TextCN;
	CEdit * pEdt_CN = (CEdit *)GetDlgItem(IDC_CN);
	pEdt_CN->GetWindowTextW(TextCN);

	CString TextC;
	CEdit * pEdt_COMPANY = (CEdit *)GetDlgItem(IDC_COMPANY);
	pEdt_COMPANY->GetWindowTextW(TextC);

	CString TextOU;
	CEdit * pEdt_OU = (CEdit *)GetDlgItem(IDC_OU);
	pEdt_OU->GetWindowTextW(TextOU);

	CString TextEmail;
	CEdit * pEdt_EMAIL = (CEdit *)GetDlgItem(IDC_EMAIL);
	pEdt_EMAIL->GetWindowTextW(TextEmail);

	CString TextS;
	CEdit * pEdt_CITY = (CEdit *)GetDlgItem(IDC_CITY);
	pEdt_CITY->GetWindowTextW(TextS);

	CString TextPassword;
	CEdit * pEdt_PASSW = (CEdit *)GetDlgItem(IDC_PASSWORD);
	pEdt_PASSW->GetWindowTextW(TextPassword);

	// ���� ������� ������������ ��������, �� ���������� ���������� ��� ������� ����� ���� � ������� ����.
	// ������������� ���� ������ � ��� ������, ���� ���������� ��� ���������
	if (TextCN == L""&&CN_OK)
	{
		CN_OK=false;
		pEdt_CN->Invalidate();
	}
	else if(TextCN != L"" && CN_OK==false)
	{
		CN_OK=true;
		pEdt_CN->Invalidate();
	}

	if (TextC == L"" && C_OK)
	{
		C_OK=false;
		pEdt_COMPANY->Invalidate();
	}
	else if(TextC != L"" && C_OK==false)
	{
		C_OK=true;
		pEdt_COMPANY->Invalidate();
	}

	if (TextOU == L"" && OU_OK)
	{
		OU_OK = false;
		pEdt_OU->Invalidate();
	}
	else if(TextOU != L"" && !OU_OK)
	{
		OU_OK = true;
		pEdt_OU->Invalidate();
	}

	if (TextS == L"" && ST_OK)
	{
		ST_OK = false;
		pEdt_CITY->Invalidate();
	}
	else if(TextS != L"" && !ST_OK)
	{
		ST_OK = true;
		pEdt_CITY->Invalidate();
	}

	//���������� �������� ��������� ������
	CString mess;
	bool PassChecked = CWork::CheckPass(TextPassword, mess);

	if (!PassChecked && PASS_OK)
	{
		PASS_OK = false;
		pEdt_PASSW->Invalidate();
	}
	else if(!PASS_OK && PassChecked)
	{
		PASS_OK = true;
		pEdt_PASSW->Invalidate();
	}

	// ���� �� ������� ���� �� ���� ����, ������� ��������������� ��������� � ��������� ����������
	if (TextCN == L"" || TextC == L"" || TextOU == L"" || TextS == L"" || TextPassword == L"")
	{
		CWnd::MessageBox(_T("���������� ��������� ��� ����, ���������� *, ��� �������� �������!"), _T("�� ��������� ����!"), 
			MB_ICONERROR | MB_OK);

		return;
	}

	// ���� email ��������������, �� ���� �������, ����� ��������� ��� ��������
	if (TextEmail != L"")
	{
		// ������� email ����������, ���� �������� "@", � ����� ����� "@" ������� ��������, ���������� "."

		bool goodemail=false;
		int SymbolAt = TextEmail.Find(L"@");

		if (SymbolAt != -1)
		{
			int SymbolDot = TextEmail.Find(L".",SymbolAt);

			if (SymbolDot!=-1 && (SymbolDot+1)<TextEmail.GetLength())
				goodemail = true;
		}

		if (!goodemail)
		{
			if(E_OK)
			{
				E_OK=false;
				pEdt_EMAIL->Invalidate();
			}

			CWnd::MessageBox(_T("���� email ��������� �����������!"), _T("����������� email!"), MB_ICONERROR | MB_OK);
			return;
		}
		else if(!E_OK)
		{
			E_OK=true;
			pEdt_EMAIL->Invalidate();
		}
	}
	else if(!E_OK)
	{
		E_OK=true;
		pEdt_EMAIL->Invalidate();
	}

	// ���� ������������ ������, ������� ���������
	if (!PassChecked)
	{
		PASS_OK = false;
		pEdt_CITY->Invalidate();

		CWnd::MessageBox(mess, _T("����� ������������ ������!"), MB_ICONERROR | MB_OK);
		return;
	}

	// �������� ������� ���������� ���������
	wchar_t currentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, currentDir);

	wstring wFolder = currentDir;
	string Folder(wFolder.begin(), wFolder.end());

	if(Folder[Folder.size()-1] != '\\')
	{
		Folder = Folder+"\\";
	}

	USES_CONVERSION;

	// ���� ��� ���� ������� ���������, �� ��������� �� � ������� ���������� � �����
	fstream FieldsFile;
	FieldsFile.open(Folder + "FieldsMapping", std::ios::out|std::ios::trunc);

	FieldsFile<<W2A(TextCN)<<endl<<W2A(TextC)<<endl<<W2A(TextOU)<<endl<<W2A(TextS)<<endl<<W2A(TextEmail);

	FieldsFile.close();

	// ��������� �������� ����� � ������� � ������ ������
	AfxBeginThread(CreateCSRThread, this, THREAD_PRIORITY_NORMAL, 0, 0, NULL);
}

// ����� ��� ������� ��������� ����� � ������� � ������ ������
UINT CreateCSRThread(LPVOID pParam)
{
	//�������� ������ �� ��������� ��� �������� � ���
	CTab1* m_tab1 = (CTab1*)pParam;

	//�������� ������ �� �������� � ��������� �� �� ����� ��������
	CButton* ButtonCSR = (CButton*)m_tab1->GetDlgItem(IDC_BUTTON1);
	ButtonCSR->EnableWindow(0);

	CEdit* pEdt_CN = (CEdit*)m_tab1->GetDlgItem(IDC_CN);
	pEdt_CN->EnableWindow(0);

	CEdit* pEdt_COMPANY = (CEdit*)m_tab1->GetDlgItem(IDC_COMPANY);
	pEdt_COMPANY->EnableWindow(0);

	CEdit* pEdt_OU = (CEdit*)m_tab1->GetDlgItem(IDC_OU);
	pEdt_OU->EnableWindow(0);

	CEdit* pEdt_EMAIL = (CEdit*)m_tab1->GetDlgItem(IDC_EMAIL);
	pEdt_EMAIL->EnableWindow(0);

	CEdit* pEdt_CITY = (CEdit*)m_tab1->GetDlgItem(IDC_CITY);
	pEdt_CITY->EnableWindow(0);

	CEdit* pEdt_PASSW = (CEdit*)m_tab1->GetDlgItem(IDC_PASSWORD);
	pEdt_PASSW->EnableWindow(0);

	CButton* pRad2 = (CButton*)m_tab1->GetDlgItem(IDC_RADIO2);
	pRad2->EnableWindow(0);

	CButton* pRad1 = (CButton*)m_tab1->GetDlgItem(IDC_RADIO1);
	pRad1->EnableWindow(0);

	//�������� ����� �����
	int keyLength = pRad2->GetCheck() != 0 ? 4096 : 2048;
	
	//�������� ������� ���������� ���������
	wchar_t currentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, currentDir);

	wstring wFolder = currentDir;
	string Folder(wFolder.begin(), wFolder.end());

	if(Folder[Folder.size()-1] != '\\')
	{
		Folder = Folder+"\\";
	}

	Folder += "GenerateRequest\\";
	
	//������ ����������. ��� ��������� ����� ����� �������� � ����.
	CWork::CheckFolder(Folder);

	//�������� �������� �������� �����
	CString TextCN;
	pEdt_CN->GetWindowTextW(TextCN);

	CString TextC;
	pEdt_COMPANY->GetWindowTextW(TextC);

	CString TextOU;
	pEdt_OU->GetWindowTextW(TextOU);

	CString TextEmail;
	pEdt_EMAIL->GetWindowTextW(TextEmail);

	CString TextS;
	pEdt_CITY->GetWindowTextW(TextS);

	CString TextPassword;
	pEdt_PASSW->GetWindowTextW(TextPassword);

	//��� �������������� CString � char*
	USES_CONVERSION;

	// �� ������� ������ �� ����� ������ ��������� ��������� ������ ����������� ��� ������ ������...
	// ��������� ��������� � ��������� ����������
	string CSR = OpenSSLManip::CreateCSR(Folder, keyLength, W2A(TextCN), 
		W2A(TextC), W2A(TextOU), W2A(TextS), 
		TextEmail == L""?nullptr:W2A(TextEmail), "RU", W2A(TextPassword));

	// ������������ ���������
	ButtonCSR->EnableWindow(1);
	pEdt_CN->EnableWindow(1);
	pEdt_COMPANY->EnableWindow(1);
	pEdt_OU->EnableWindow(1);
	pEdt_EMAIL->EnableWindow(1);
	pEdt_CITY->EnableWindow(1);
	pEdt_PASSW->EnableWindow(1);
	pRad2->EnableWindow(1);
	pRad1->EnableWindow(1);

	// ����������� ������ � �������� ��� ����������� �����������
	CString CCSR(CSR.c_str());
	CCSR.Replace(L"\n",L"\r\n");

	CEdit * pEdt_EDIT8 = (CEdit *)m_tab1->GetDlgItem(IDC_CSR);
	pEdt_EDIT8->SetWindowTextW(CCSR);

	return 0;
}

void CTab1::OnEnSetfocusEdit2()
{
	CString temp;
	CEdit * pEdt = (CEdit *)GetDlgItem(IDC_CN);
	pEdt->GetWindowTextW(temp);
	if(temp==L"������ �.�.")
	{
		pEdt->SetWindowTextW(L"");
	}
}


void CTab1::OnEnKillfocusEdit2()
{
	CString temp;
	CEdit * pEdt = (CEdit *)GetDlgItem(IDC_CN);
	pEdt->GetWindowTextW(temp);
	if(temp==L"")
	{
		pEdt->SetWindowTextW(L"������ �.�.");
	}
}

// ����� ��� ������� �����
HBRUSH CTab1::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	// ����� ������������� ������
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// ���� ������������ ������ ��� ������� � ��� �������� ���� ������� �����������, �� ��������� HBRUSH ��� ��������� �����
	switch(pWnd->GetDlgCtrlID())
	{
	case IDC_CN:
		{
			if(!CN_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
	case IDC_OU:
		{
			if(!OU_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
	case IDC_COMPANY:
		{
			if(!C_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
	case IDC_CITY:
		{
			if(!ST_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
	case IDC_PASSWORD:
		{
			if(!PASS_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
	case IDC_EMAIL:
		{
			if(!E_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
		// ��� ����� �������� ������ ����� ����� - ��� ����� ����� �����
	case IDC_CSR:
		{
			pDC->SetBkColor(m_normalcolor);
			hbr = (HBRUSH)m_normalbrush;
		}
		break;
	}

	return hbr;
}

// �� ������� ShowWindow, ���� ����� ��� � ���������� SW_SHOW, �������� �������� ������ �� ���������� ������������� ����
void CTab1::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	CCertificateRequestDlg* wnd = (CCertificateRequestDlg*)AfxGetMainWnd();

	CEdit* pEdt_PASSW = (CEdit*)this->GetDlgItem(IDC_PASSWORD);

	if(bShow)
	{
		pEdt_PASSW->SetWindowTextW(wnd->Password);
	}
}

// ������� ��� ������ �������� ������� ����� ���������
// ��� ��������� �������� �������� ��� � ����������� ���������� ������������� ����
void CTab1::OnEnChangePassword()
{
	CCertificateRequestDlg* wnd = (CCertificateRequestDlg*)AfxGetMainWnd();
	
	CEdit* pEdt_PASSW = (CEdit*)this->GetDlgItem(IDC_PASSWORD);

	CString Password;
	pEdt_PASSW->GetWindowTextW(Password);

	wnd->Password = Password;
}
