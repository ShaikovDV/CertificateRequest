//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CertificateRequest.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CERTIFICATEREQUEST_DIALOG   102
#define IDD_RESOURCE_TAB1               105
#define IDD_OLE_PROPPAGE_LARGE          106
#define IDD_RESOURCE_TAB2               106
#define IDR_MAINFRAME                   128
#define IDC_TAB1                        1000
#define IDC_CN                          1002
#define IDC_COMPANY                     1003
#define IDC_OU                          1004
#define IDC_PASSWORD                    1005
#define IDC_EMAIL                       1006
#define IDC_CITY                        1007
#define IDC_RADIO1                      1008
#define IDC_RADIO2                      1009
#define IDC_CSR                         1010
#define IDC_BUTTON1                     1011
#define IDC_CERTFNAME                   1012
#define IDC_BUTTON2                     1013
#define IDC_PKPASS                      1014
#define IDC_PKEY                        1015
#define IDC_BUTTON3                     1016
#define IDC_MESSAGE                     1017
#define IDC_PFXMESSAGE                  1018
#define IDC_BUTTON4                     1019

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1021
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
