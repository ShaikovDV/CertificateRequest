#pragma once
#include "afxwin.h"
#include <string>


// CTab1 dialog

class CTab1 : public CDialogEx
{
	DECLARE_DYNAMIC(CTab1)

public:
	CTab1(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTab1();

// Dialog Data
	enum { IDD = IDD_RESOURCE_TAB1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	CBrush m_redbrush,m_normalbrush;
	COLORREF m_redcolor,m_normalcolor;

	bool CN_OK, OU_OK, C_OK, ST_OK, E_OK, PASS_OK;

public:
	
	afx_msg BOOL OnInitDialog();

	afx_msg void OnBnClickedButton1();
	afx_msg void OnEnSetfocusEdit2();
	afx_msg void OnEnKillfocusEdit2();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnEnChangePassword();
};
