#pragma once


// CTab2 dialog

class CTab2 : public CDialogEx
{
	DECLARE_DYNAMIC(CTab2)

public:
	CTab2(CWnd* pParent = NULL);   // standard constructor
	virtual ~CTab2();

// Dialog Data
	enum { IDD = IDD_RESOURCE_TAB2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()

	CBrush m_redbrush,m_normalbrush;
	COLORREF m_redcolor,m_normalcolor;

	bool CERTFNAME_OK, PKEYFNAME_OK, PASS_OK;

public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	afx_msg void OnBnClickedButton3();
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
	afx_msg BOOL OnInitDialog();
	afx_msg void OnBnClickedButton4();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnEnChangePkpass();
};
