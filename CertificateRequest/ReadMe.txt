﻿Приложение Certificate Request.

Данное приложение создано для генерации закрытого ключа RSA и запроса на сертификат на стороне конечного пользователя.

Приложение создано с использованием библиотеки OpenSSL, созданной Eric Young и Tim Hudson.

Ссылка на лицензию OpenSSL: https://www.openssl.org/source/license.html