#include "StdAfx.h"
#include "Work.h"


// ���� ���������� �� ����������, ������ �
void CWork::CheckFolder(string Folder)
{
	DWORD FolderAttr = GetFileAttributesA(Folder.c_str());
	if (FolderAttr == INVALID_FILE_ATTRIBUTES)
	{
		CreateDirectoryA(Folder.c_str(), NULL);
		return;
	}
}

// ����� ��� �������� ������������ ������.
// ������� ������ ���������� ���� � ��� �� ����� 6 �������� � �� �������� ������ ����������� �������.
bool CWork::CheckPass(CString pass, CString& message)
{
	if (pass.GetLength() < 6)
	{
		message = L"����� ������ ������ 6 ��������";
		return false;
	}
	const CString AllowedSymbols = L"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()_-1234567890+=/\"\\,?'|;:~`.{}[]<>";
	for(int i=0;i<pass.GetLength();i++)
	{
		if (AllowedSymbols.Find(pass.Mid(i, 1))==-1)
		{
			message = L"� ������ ����������� ������������ ������ ������� ���������� ��������, ����� � ��������� �������.";
			return false;
		}
	}

	message = L"";
	return true;
}
