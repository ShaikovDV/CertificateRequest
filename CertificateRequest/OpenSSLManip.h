
#ifndef HEADER_OPENSSLMANIP
#define HEADER_OPENSSLMANIP

#include <string>

using namespace std;

class OpenSSLManip
{
public:
	//OpenSSLManip(void);
	//~OpenSSLManip(void);

	static string CreateCSR(string FolderName, int bits, const char* CommonName, const char* Organization, const char* OrganizationUnit,
							const char* City, const char* Email, const char* Country, const char* PKeyPassword);

	static unsigned char* translateToUTF8(const unsigned char* str);

	static string CreatePFX(string FolderName, char* PKeyFileName, char* Password, char* X509CertFileName, string& messages);
};


#endif

