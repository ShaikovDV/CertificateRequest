
// CertificateRequestDlg.cpp : ���� ����������
//

#include "stdafx.h"
#include "CertificateRequest.h"
#include "CertificateRequestDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// ���������� ���� CAboutDlg ������������ ��� �������� �������� � ����������

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// ������ ����������� ����
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // ��������� DDX/DDV

// ����������
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// ���������� ���� CCertificateRequestDlg




CCertificateRequestDlg::CCertificateRequestDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(CCertificateRequestDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCertificateRequestDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB1, m_tab1);
}

BEGIN_MESSAGE_MAP(CCertificateRequestDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB1, &CCertificateRequestDlg::OnTcnSelchangeTab1)
END_MESSAGE_MAP()


// ����������� ��������� CCertificateRequestDlg

BOOL CCertificateRequestDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// ���������� ������ "� ���������..." � ��������� ����.

	// IDM_ABOUTBOX ������ ���� � �������� ��������� �������.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// ������ ������ ��� ����� ����������� ����. ����� ������ ��� �������������,
	//  ���� ������� ���� ���������� �� �������� ����������
	SetIcon(m_hIcon, TRUE);			// ������� ������
	SetIcon(m_hIcon, FALSE);		// ������ ������


	// �������� TabControl
	CTabCtrl* pTabCtrl = (CTabCtrl*)GetDlgItem(IDC_TAB1);

	// ������ ������� � ��������� �� �� �������

	m_r_tab1.Create(IDD_RESOURCE_TAB1, pTabCtrl);

	TCITEM item1;
	item1.mask = TCIF_TEXT | TCIF_PARAM;
	item1.lParam = (LPARAM) &m_r_tab1;
	item1.pszText = _T("���1. �������� ������� �� ����������");

	// ��������� ������ ������� ��� ������� 0
	pTabCtrl->InsertItem(0, &item1);

	CRect rcItem;
	pTabCtrl->GetItemRect(0, &rcItem);
	m_r_tab1.SetWindowPos(NULL, rcItem.left, rcItem.bottom+1,0,0,SWP_NOSIZE | SWP_NOZORDER);

	// ���������� �������� ������ �������
	m_r_tab1.ShowWindow(SW_SHOW);

	m_r_tab2.Create(IDD_RESOURCE_TAB2, pTabCtrl);

	TCITEM item2;
	item2.mask = TCIF_TEXT | TCIF_PARAM;
	item2.lParam = (LPARAM) &m_r_tab2;
	item2.pszText = _T("���2. �������� ����������� � ������� PFX/P12");

	// ��������� ������ ��� ������� 1
	pTabCtrl->InsertItem(1, &item2);

	CRect rcItem2;
	pTabCtrl->GetItemRect(0, &rcItem2);
	m_r_tab2.SetWindowPos(NULL, rcItem2.left, rcItem2.bottom+1,0,0,SWP_NOSIZE | SWP_NOZORDER);

	//m_r_tab2.ShowWindow(SW_SHOW);

	return TRUE;  // ������� �������� TRUE, ���� ����� �� ������� �������� ����������
}

void CCertificateRequestDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// ��� ���������� ������ ����������� � ���������� ���� ����� ��������������� ����������� ���� �����,
//  ����� ���������� ������. ��� ���������� MFC, ������������ ������ ���������� ��� �������������,
//  ��� ������������� ����������� ������� ��������.

void CCertificateRequestDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // �������� ���������� ��� ���������

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// ������������ ������ �� ������ ����������� ��������������
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// ��������� ������
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// ������� �������� ��� ������� ��� ��������� ����������� ������� ��� �����������
//  ���������� ����.
HCURSOR CCertificateRequestDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


// ����� �������
void CCertificateRequestDlg::OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult)
{
	// �������� ������� ��������� TabControl-�
	CTabCtrl* pTabCtrl = (CTabCtrl*)GetDlgItem(IDC_TAB1);
	
	// �������� ����� ��������� �������
	int selectedTab = pTabCtrl->GetCurSel();

	// ������ �� ������ ���������� ��������� � ������ ��������� �������
	switch(selectedTab)
	{
	case 0:
		{
			m_r_tab1.ShowWindow(SW_SHOW);
			m_r_tab2.ShowWindow(SW_HIDE);
		}
		break;
	case 1:
		{
			m_r_tab1.ShowWindow(SW_HIDE);
			m_r_tab2.ShowWindow(SW_SHOW);
		}
		break;
	}

	*pResult = 0;
}
