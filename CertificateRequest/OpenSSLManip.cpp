#include "StdAfx.h"
#include <string>

#include "OpenSSLManip.h"
#include "openssl/rsa.h"
#include "openssl/pem.h"
#include "openssl/err.h"
#include "openssl/pkcs12.h"
#include <fstream>

using namespace std;


string OpenSSLManip::CreateCSR(string FolderName, int bits, const char* CommonName, const char* Organization, const char* OrganizationUnit,
	const char* City, const char* Email, const char* Country, const char* PKeyPassword)
{
	RSA             *r = NULL;
	BIGNUM          *bne = NULL;
	string ReturnString;
	int ret = 0;

	int             nVersion = 3;
	unsigned long   e = RSA_F4;

	X509_REQ        *x509_req = NULL;
	X509_name_st    *x509_name = NULL;
	EVP_PKEY        *pKey = NULL;
	
	BIO*            out_pem = NULL;
	BIO*			bioMemory_CSR = NULL;

	OpenSSL_add_all_algorithms(); // ����� OPENSSL!!!

	const unsigned char* u8Email = nullptr;

	const unsigned char*	u8CommonName		= translateToUTF8((const unsigned char*)CommonName);
	const unsigned char*	u8Organization		= translateToUTF8((const unsigned char*)Organization);
	const unsigned char*	u8OrganizationUnit	= translateToUTF8((const unsigned char*)OrganizationUnit);
	const unsigned char*	u8City				= translateToUTF8((const unsigned char*)City);
	const unsigned char*	u8Country			= translateToUTF8((const unsigned char*)Country);

	if(Email!=nullptr)
		u8Email				= translateToUTF8((const unsigned char*)Email);

	try
	{
		bne = BN_new();
		ret = BN_set_word(bne,e);
		if(ret!=1)
		{
			throw "������ ��� ������������� �������� �����.";
		}

		r = RSA_new();
		ret = RSA_generate_key_ex(r, bits, bne, NULL);
		if(ret != 1)
		{
			throw "������ ��� ��������� ������.";
		}

		x509_req = X509_REQ_new();
		ret = X509_REQ_set_version(x509_req, nVersion);
		if (ret != 1)
		{
			throw "������ ��� �������� ������� �������.";
		}

		x509_name = X509_REQ_get_subject_name(x509_req);

		ret = X509_NAME_add_entry_by_txt(x509_name,"C", MBSTRING_UTF8, u8Country, -1, -1, 0);
		if (ret != 1)
		{
			throw "������ ��� ���������� Country � �������.";
		}

		ret = X509_NAME_add_entry_by_txt(x509_name,"ST", MBSTRING_UTF8, u8City, -1, -1, 0);
		if (ret != 1)
		{
			throw "������ ��� ���������� City � �������.";
		}
		if(Email!=nullptr)
		{
			ret = X509_NAME_add_entry_by_txt(x509_name,"emailAddress", MBSTRING_UTF8, u8Email, -1, -1, 0);
			if (ret != 1)
			{
				throw "������ ��� ���������� Email � �������.";
			}
		}
		ret = X509_NAME_add_entry_by_txt(x509_name,"OU", MBSTRING_UTF8, u8OrganizationUnit, -1, -1, 0);
		if (ret != 1)
		{
			throw "������ ��� ���������� OrganizationUnit � �������.";
		}
		ret = X509_NAME_add_entry_by_txt(x509_name,"O", MBSTRING_UTF8, u8Organization, -1, -1, 0);
		if (ret != 1)
		{
			throw "������ ��� ���������� Organization � �������.";
		}
		ret = X509_NAME_add_entry_by_txt(x509_name,"CN", MBSTRING_UTF8, u8CommonName, -1, -1, 0);	
		if (ret != 1)
		{
			throw "������ ��� ���������� CommonName � �������.";
		}

		pKey = EVP_PKEY_new();

		EVP_PKEY_assign_RSA(pKey, r);

		ret = X509_REQ_set_pubkey(x509_req, pKey);

		if(ret != 1)
		{
			throw "������ ��� ���������� ��������� ����� � �������.";
		}

		ret = X509_REQ_sign(x509_req, pKey, EVP_sha1());

		if(ret <= 0)
		{
			throw "������ ��� ������� �������.";
		}

		if(FolderName[FolderName.size()-1] != '\\')
		{
			FolderName = FolderName+"\\";
		}
		string FilePKey = FolderName + "PKey.pem";		

		out_pem = BIO_new_file(FilePKey.c_str(),"w");

		ret = PEM_write_bio_PKCS8PrivateKey(out_pem, pKey, EVP_aes_256_cbc(), NULL, 0, 0, (void*)PKeyPassword);

		if(ret != 1)
		{
			throw "������ ��� ������ ��������� �����.";
		}

		

		string FileCSR = FolderName + "Request.csr";

		bioMemory_CSR = BIO_new(BIO_s_mem());

		ret = PEM_write_bio_X509_REQ_NEW(bioMemory_CSR, x509_req);

		if(ret != 1)
		{
			throw "������ ��� ������ �������.";
		}

		// �������� ������ �� ������ � ����, �� � ���������� ��� ��� ����������� �� ����������

		int CSRlen = BIO_pending(bioMemory_CSR);
		char* CSR = (char*)calloc(CSRlen+1, 1); 
		BIO_read(bioMemory_CSR, CSR, CSRlen);

		std::fstream pKeyFile;
		pKeyFile.open(FileCSR.c_str(), std::ios::trunc|std::ios::out);
		pKeyFile<<CSR;
		pKeyFile.close();

		ReturnString = CSR;

		free(CSR);
	}
	catch(const char* Message)
	{
		ReturnString = "��������� ������: \"";// + Message + "\"";
		ReturnString.append(Message);
		ReturnString.append("\"��������� �����:");

		char* buf = new char[500];
		unsigned long ERRCODE = 0;
		while((ERRCODE = ERR_get_error())!=0)
		{
			ReturnString.append("\r\n");
			ERR_error_string_n(ERRCODE, buf, 500);

			ReturnString.append(buf);
		}
		delete[] buf;
	}

	
	
	delete[] u8City;
	delete[] u8CommonName;
	delete[] u8Country;
	if(Email!=nullptr)
		delete[] u8Email;
	delete[] u8Organization;
	delete[] u8OrganizationUnit;

    X509_REQ_free(x509_req);

    BIO_free_all(out_pem);
	BIO_free_all(bioMemory_CSR);

    EVP_PKEY_free(pKey);
    BN_free(bne);

	return ReturnString;
}

unsigned char* OpenSSLManip::translateToUTF8(const unsigned char* str)
{
	const unsigned char* letter = (const unsigned char*)"������������������������������������������������������������������";

	const char* code = "D090D091D092D093D094D095D096D097D098D099D09AD09BD09CD09DD09ED09FD0A0D0A1D0A2D0A3D0A4D0A5D0A6D0A7D0A8D0A9D0AAD0ABD0ACD0ADD0AED0AFD0B0D0B1D0B2D0B3D0B4D0B5D0B6D0B7D0B8D0B9D0BAD0BBD0BCD0BDD0BED0BFD180D181D182D183D184D185D186D187D188D189D18AD18BD18CD18DD18ED18FD081D191";

	unsigned char* newText=new unsigned char[strlen((char*)str)*2+1];
	int k=0;
	for(size_t i=0;i<strlen((char*)str);i++)
	{
		if(str[i]<0x7f)
		{
			newText[k++] = str[i];
			continue;
		}
		for(int j=0;letter[j]!=0;j++)
		{
			if(str[i]==letter[j])
			{
				char temp[3];
				temp[0] = code[j*4];
				temp[1] = code[j*4+1];
				temp[2] = 0;

				long tempLong = strtol(temp, 0, 16);

				newText[k++] = (char)tempLong;

				temp[0] = code[j*4+2];
				temp[1] = code[j*4+3];
				temp[2] = 0;
				tempLong = strtol(temp, 0, 16);

				newText[k++] = (char)tempLong;

				break;
			}
		}
	}
	newText[k]=0;
	return newText;
}

string OpenSSLManip::CreatePFX(string FolderName, char* PKeyFileName, char* Password, char* X509CertFileName, string& messages)
{
	unsigned long ERRCODE = 0;
	char* buf = new char[500];
	bool error = false;

	EVP_PKEY* PKey;
	X509* Cert;
	PKCS12* P12;

	OpenSSL_add_all_algorithms(); // ����� OPENSSL!!!


	BIO* KEYFile = BIO_new_file(PKeyFileName, "r");

	PKey = PEM_read_bio_PrivateKey(KEYFile, NULL, NULL, Password);

	BIO_free_all(KEYFile);
	
	while((ERRCODE = ERR_get_error())!=0)
	{
		if(error) messages.append("\r\n");
		else error = true;
		
		ERR_error_string_n(ERRCODE, buf, 500);

		messages.append(buf);
	}

	if(error) 
	{
		return "������ ������ ����� ��������� �����. ��������� ������������ �������� ������.";
	}

	BIO* CertFile = BIO_new_file(X509CertFileName, "r");

	Cert = PEM_read_bio_X509(CertFile, NULL, NULL, NULL);

	BIO_free_all(CertFile);

	while((ERRCODE = ERR_get_error())!=0)
	{
		if(error) messages.append("\r\n");
		else error = true;
		
		ERR_error_string_n(ERRCODE, buf, 500);

		messages.append(buf);
	}

	if(error) return "������ ������ ����� �����������. ��������� ������������ �������� �����.";

	P12 = PKCS12_create(Password, NULL, PKey, Cert, 0, 0, 0, 0, 0, 0);

	while((ERRCODE = ERR_get_error())!=0)
	{
		if(error) messages.append("\r\n");
		else error = true;
		
		ERR_error_string_n(ERRCODE, buf, 500);

		messages.append(buf);
	}

	if(error) return "������ �������� ����� � ������� PKCS12.";

	if(FolderName[FolderName.size()-1] != '\\')
	{
		FolderName = FolderName+"\\";
	}

	BIO* PKCS12_file = BIO_new_file((FolderName + "CertFile.p12").c_str(), "w");

	i2d_PKCS12_bio(PKCS12_file, P12);

	BIO_free_all(PKCS12_file);

	while((ERRCODE = ERR_get_error())!=0)
	{
		if(error) messages.append("\r\n");
		else error = true;
		
		ERR_error_string_n(ERRCODE, buf, 500);

		messages.append(buf);
	}

	if(error) return "������ ������ ����� P12 �� ����.";

	return "";
}
