
// CertificateRequestDlg.h : ���� ���������
//

#pragma once
#include "afxcmn.h"
#include "Tab1.h"
#include "Tab2.h"


// ���������� ���� CCertificateRequestDlg
class CCertificateRequestDlg : public CDialogEx
{
// ��������
public:
	CCertificateRequestDlg(CWnd* pParent = NULL);	// ����������� �����������

// ������ ����������� ����
	enum { IDD = IDD_CERTIFICATEREQUEST_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// ��������� DDX/DDV


// ����������
protected:
	HICON m_hIcon;

	// ��������� ������� ����� ���������
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CTabCtrl m_tab1;

	CTab1 m_r_tab1;
	CTab2 m_r_tab2;
	afx_msg void OnTcnSelchangeTab1(NMHDR *pNMHDR, LRESULT *pResult);

	CString Password;
};
