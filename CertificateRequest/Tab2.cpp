// Tab2.cpp : implementation file
//

#include "stdafx.h"
#include "CertificateRequest.h"
#include "Tab2.h"
#include "CertificateRequestDlg.h"
#include "afxdialogex.h"
#include "OpenSSLManip.h"
#include "Work.h"


// Tab2 dialog

IMPLEMENT_DYNAMIC(CTab2, CDialogEx)

CTab2::CTab2(CWnd* pParent /*=NULL*/)
	: CDialogEx(CTab2::IDD, pParent)
{

}

CTab2::~CTab2()
{
}

void CTab2::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CTab2, CDialogEx)
	ON_BN_CLICKED(IDC_BUTTON1, &CTab2::OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON2, &CTab2::OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON3, &CTab2::OnBnClickedButton3)
	ON_WM_CTLCOLOR()
	ON_BN_CLICKED(IDC_BUTTON4, &CTab2::OnBnClickedButton4)
	ON_WM_SHOWWINDOW()
	ON_EN_CHANGE(IDC_PKPASS, &CTab2::OnEnChangePkpass)
END_MESSAGE_MAP()

// Tab2 message handlers

BOOL CTab2::OnInitDialog()
{
	// ���������� �������� ���������� � true ��� ������ � ����������� ����
	CERTFNAME_OK=PKEYFNAME_OK=PASS_OK=true;

	// ����������� ������ �� ���������

	m_normalcolor = RGB(255,255,255);
	m_redcolor = RGB(255,160,122);

	m_normalbrush.CreateSolidBrush(m_normalcolor);
	m_redbrush.CreateSolidBrush(m_redcolor);

	return true;
}

// ����� ����� ����������� � �����
void CTab2::OnBnClickedButton1()
{
	CString fileName;
	wchar_t* p = fileName.GetBuffer( MAX_PATH + 2 );
	CFileDialog dlgFile(TRUE);
	OPENFILENAME& ofn = dlgFile.GetOFN( );

	ofn.lpstrFile = p;
	ofn.nMaxFile = MAX_PATH + 2;
	ofn.lpstrFilter = L"���� �����������\0*.cer;*.crt\0\0";

	if(dlgFile.DoModal()== IDOK)
	{
		CEdit * pCertName = (CEdit *)GetDlgItem(IDC_CERTFNAME);
		pCertName->SetWindowTextW(p);
	}
	fileName.ReleaseBuffer();	
	CERTFNAME_OK=true;
	CEdit * pEdt_CERTFNAME = (CEdit *)GetDlgItem(IDC_CERTFNAME);
	pEdt_CERTFNAME->Invalidate();
}

// ����� ����� � �������� ������ � �����
void CTab2::OnBnClickedButton2()
{
	CString fileName;
	wchar_t* p = fileName.GetBuffer( MAX_PATH + 2 );
	CFileDialog dlgFile(TRUE);
	OPENFILENAME& ofn = dlgFile.GetOFN( );

	ofn.lpstrFile = p;
	ofn.nMaxFile = MAX_PATH + 2;
	ofn.lpstrFilter = L"���� pem\0*.pem\0\0";

	if(dlgFile.DoModal()== IDOK)
	{
		CEdit * pCertName = (CEdit *)GetDlgItem(IDC_PKEY);
		pCertName->SetWindowTextW(p);
	}
	fileName.ReleaseBuffer();

	PKEYFNAME_OK=true;
	CEdit * pEdt_PKEY = (CEdit *)GetDlgItem(IDC_PKEY);
	pEdt_PKEY->Invalidate();
}


void CTab2::OnBnClickedButton3()
{
	// �������� ������ �� �������� ���������� � �������� ��������
	CString CERTFNAME;
	CEdit * pEdt_CERTFNAME = (CEdit *)GetDlgItem(IDC_CERTFNAME);
	pEdt_CERTFNAME->GetWindowTextW(CERTFNAME);

	CString PKEY;
	CEdit * pEdt_PKEY = (CEdit *)GetDlgItem(IDC_PKEY);
	pEdt_PKEY->GetWindowTextW(PKEY);

	CString PASSWORD;
	CEdit * pEdt_PASS = (CEdit *)GetDlgItem(IDC_PKPASS);
	pEdt_PASS->GetWindowTextW(PASSWORD);
	
	//PASS_OK

	// ���� ������� ������������ ��������, �� ���������� ���������� ��� ������� ����� ���� � ������� ����.
	if (CERTFNAME == L""&&CERTFNAME_OK)
	{
		CERTFNAME_OK=false;
		pEdt_CERTFNAME->Invalidate();
	}

	if (PKEY == L"" && PKEYFNAME_OK)
	{
		PKEYFNAME_OK=false;
		pEdt_PKEY->Invalidate();
	}

	if (PASSWORD == L"" && PASS_OK)
	{
		PASS_OK=false;
		pEdt_PASS->Invalidate();
	}
	else if(PASSWORD != L"" && !PASS_OK)
	{
		PASS_OK=true;
		pEdt_PASS->Invalidate();
	}

	if(CERTFNAME == L""||PKEY == L"")
	{
		CWnd::MessageBox(_T("���������� ��������� ��� ����!"), _T("�� ��������� ����!"), 
			MB_ICONERROR | MB_OK);
		return;
	}

	//�������� ������� ���������� ���������
	wchar_t currentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, currentDir);

	wstring wFolder = currentDir;
	string Folder(wFolder.begin(), wFolder.end());

	if(Folder[Folder.size()-1] != '\\')
	{
		Folder = Folder+"\\";
	}

	Folder += "GenerateRequest\\";
	
	//������ ����������. ��� ��������� ����� ����� �������� � ����.
	CWork::CheckFolder(Folder);

	//��� �������������� CString � char*
	USES_CONVERSION;

	string OpenSSLmessages;

	// �������� ����� ��� �������� ����� ����������� � ������� PKCS12
	// ������ ����������� ����� ��������� � ������� ��������� �����
	string PFXmessage = OpenSSLManip::CreatePFX(Folder, W2A(PKEY), W2A(PASSWORD), W2A(CERTFNAME), OpenSSLmessages);
	
	CString CPFXmessage(PFXmessage.c_str());
	
	CEdit* pEdt_MESS = (CEdit*)GetDlgItem(IDC_PFXMESSAGE);

	// ���� � ���������� ���������� �� ������ ������, �� ��������� ������
	if(PFXmessage!="")
	{		
		CString COpenSSLmessages(PFXmessage.c_str());
		
		COpenSSLmessages.Append(L"\r\n����������� ������:\r\n");
		
		CString Mes(OpenSSLmessages.c_str());
		
		COpenSSLmessages.Append(Mes);
		
		// ������� ��������� �� ������ � ��� OpenSSL
		pEdt_MESS->SetWindowTextW(COpenSSLmessages);

		CWnd::MessageBox(CPFXmessage, _T("������ ��� ���������!"), 
			MB_ICONERROR | MB_OK);
	}
	else
	{
		// ���� �� ���������, �� ������� �������������� ��������� � ������ ������� ������ �� ������� ����� ������� �����.
		pEdt_MESS->SetWindowTextW(L"�������� ����� ����������� ������� PKCS12 ��������� �������!");
		CButton* pBtn4 = (CButton*)GetDlgItem(IDC_BUTTON4);
		pBtn4->ShowWindow(SW_SHOW);
	}
}

// ��������� ���������� � ���������� �������������
void CTab2::OnBnClickedButton4()
{
	wchar_t currentDir[MAX_PATH];
	GetCurrentDirectory(MAX_PATH, currentDir);

	wstring wFolder = currentDir;

	if(wFolder[wFolder.size()-1] != '\\')
	{
		wFolder = wFolder+L"\\";
	}

	wFolder += L"GenerateRequest\\";

	ShellExecute(0, L"open", L"explorer.exe", wFolder.c_str(), 0, SW_SHOW);
}

// ����� ��������� ������� �������
HBRUSH CTab2::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor)
{
	HBRUSH hbr = CDialogEx::OnCtlColor(pDC, pWnd, nCtlColor);

	// ���� ������� ������� - ���� �� ���������� ���������, � ��������� ����, �� ��������� HBRUSH
	// ��� ReadOnly �����, ���� �� � �������, ��������� ����� ���� ����� ������������ ������ - ��� �������� ������
	switch(pWnd->GetDlgCtrlID())
	{
	case IDC_CERTFNAME:
		{
			if(!CERTFNAME_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
			else
			{
				pDC->SetBkColor(m_normalcolor);
				hbr = (HBRUSH)m_normalbrush;
			}
		}
		break;
	case IDC_PKEY:
		{
			if(!PKEYFNAME_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
			else
			{
				pDC->SetBkColor(m_normalcolor);
				hbr = (HBRUSH)m_normalbrush;
			}
		}
		break;
	case IDC_PKPASS:
		{
			if(!PASS_OK)
			{
				pDC->SetBkColor(m_redcolor);
				hbr = (HBRUSH)m_redbrush;
			}
		}
		break;
	case IDC_PFXMESSAGE:
		{
			pDC->SetBkColor(m_normalcolor);
			hbr = (HBRUSH)m_normalbrush;
		}
		break;
	}
	return hbr;
}

// �� ������� ShowWindow, ���� ����� ��� � ���������� SW_SHOW, �������� �������� ������ �� ���������� ������������� ����
void CTab2::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialogEx::OnShowWindow(bShow, nStatus);

	CCertificateRequestDlg* wnd = (CCertificateRequestDlg*)AfxGetMainWnd();

	CEdit* pEdt_PASSW = (CEdit*)this->GetDlgItem(IDC_PKPASS);

	if(bShow)
	{
		pEdt_PASSW->SetWindowTextW(wnd->Password);
	}
}

// ������� ��� ������ �������� ������� ����� ���������
// ��� ��������� �������� �������� ��� � ����������� ���������� ������������� ����
void CTab2::OnEnChangePkpass()
{
	CCertificateRequestDlg* wnd = (CCertificateRequestDlg*)AfxGetMainWnd();
	
	CEdit* pEdt_PASSW = (CEdit*)this->GetDlgItem(IDC_PKPASS);

	CString Password;
	pEdt_PASSW->GetWindowTextW(Password);

	wnd->Password = Password;
}
